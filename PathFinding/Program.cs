﻿using System;
using PathFinding.Structs;

namespace PathFinding
{
    class Program
    {
        static void Main(string[] args)
        {
            int[][] map = 
            {
                new [] {0, 1, 0},
                new [] {1, 0, 1},
                new [] {0, 1, 1}
            };

            int price = GetMinTravelPrice(map);
            
            Console.WriteLine($"\nResult {price}");
        }
        
        private static int GetMinTravelPrice(int[][] cells)
        {
            int price = 0;
            Vector2 currentPos = new Vector2();

            int m = cells.Length,
                n = cells[0].Length;

            Console.Write($"[{currentPos.ToString()}");
            do
            {
                (Vector2 newPos, int price) step = GetInexpensiveDirection(currentPos, cells);

                currentPos = step.newPos;
                price += step.price;
                Console.Write($", {currentPos.ToString()}");

            } 
            while ((currentPos.y != (m - 1)) || (currentPos.x != (n - 1)));

            Console.Write($"]");

            return price;
        }
        
        /// <summary>
        /// Обсчет оптимального шага.
        /// </summary>
        /// <param name="currentPos">Текущая позиция игрока.</param>
        /// <param name="cells">Карта.</param>
        /// <returns>Возвращает картеж: направления типа (x: 0, y: 1); цена на путь.</returns>
        private static (Vector2, int) GetInexpensiveDirection(Vector2 currentPos, int[][] cells)
        {
            Vector2 newPos = new Vector2();
            Vector2 posTmp = new Vector2();
            
            int resultPrice = 0;
            
            double trueMovePrice = double.MaxValue;
            
            int m = cells.Length,
                n = cells[0].Length;
            
            int currentCellMark = cells[currentPos.y][currentPos.x];
            
            void Recalculate()
            {
                double lengthTmp;
                int priceTmp = currentCellMark == cells[posTmp.y][posTmp.x] ? 0 : 1;
                
                if (trueMovePrice > (lengthTmp = CalculateLength(posTmp, new Vector2(n-1, m-1)) + priceTmp))
                {
                    resultPrice = priceTmp;
                    trueMovePrice = lengthTmp;
                    newPos = posTmp;
                }
            }
            
            // Вверх.
            posTmp = currentPos;
            posTmp.y -= 1;
            
            if (posTmp.y >= 0 && posTmp.y < m)
            {
                Recalculate();
            }
            
            // Вправо.
            posTmp = currentPos;
            posTmp.x += 1;
            
            if (posTmp.x >= 0 && posTmp.x < n)
            {
                Recalculate();
            }
            
            // Вниз.
            posTmp = currentPos;
            posTmp.y += 1;
            
            if (posTmp.y >= 0 && posTmp.y < m)
            {
                Recalculate();
            }
            
            // Влево.
            posTmp = currentPos;
            posTmp.x += 1;
            
            if (posTmp.x >= 0 && posTmp.x < n)
            {
                Recalculate();
            }

            return (newPos, resultPrice);
        }

        private static double CalculateLength(Vector2 vec1, Vector2 vec2)
        {
            int val1 = (vec2.x - vec1.x);
            val1 *= val1;

            int val2 = (vec2.y - vec1.y);
            val2 *= val2;

            return Math.Sqrt(val1 + val2);
        }
    }
}